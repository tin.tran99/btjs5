function ketQua() {
  var tenNguoi = document.getElementById("ten_ho").value;
  var kW = document.getElementById("dung_dien").value;

  var tongTien = null;
  if (kW < 50) {
    tongTien = kW * 500;
  } else if (kW <= 100) {
    tongTien = 50 * 500 + (kW - 50) * 650;
  } else if (kW <= 200) {
    tongTien = 50 * 500 + 50 * 650 + (kW - 100) * 850;
  } else if (kW <= 350) {
    tongTien = 50 * 500 + 50 * 650 + 100 * 850 + (kW - 200) * 1100;
  } else
    tongTien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kW - 350) * 1300;

  document.getElementById("ten_nguoi").innerHTML = `Chủ hộ: ${tenNguoi}`;

  document.getElementById("ket_qua").innerHTML = `Tổng tiền điện:${tongTien} VND `;
}
