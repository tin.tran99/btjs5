// tinh diem khu vuc
var khu_vuc_A = "khu_vuc_A";
var khu_vuc_B = "khu_vuc_B";
var khu_vuc_C = "khu_vuc_C";
var tinhDiemKhuVuc = function (khuVuc) {
  switch (khuVuc) {
    case "khu_vuc_A": {
      return 2;
    }
    case "khu_vuc_B": {
      return 1;
    }
    case "khu_vuc_C": {
      return 0.5;
    }
    case "khu_vuc_D": {
      return 0;
    }
  }
};
// tinh diem doi tuong
var tinhDiemDoiTuong = function (doiTuong) {
  switch (doiTuong) {
    case "doi_tuong_1": {
      return 2.5;
    }
    case "doi_tuong_2": {
      return 1.5;
    }
    case "doi_tuong_3": {
      return 1;
    }
    case "doi_tuong_4": {
      return 0;
    }
  }
};
// lay ket qua thong qua click button
document.getElementById("ket_qua").addEventListener("click", function () {
  var diemChuan = document.getElementById("diem_chuan").value;
  var diemMon1 = document.getElementById("diem_mon_1").value;
  var diemMon2 = document.getElementById("diem_mon_2").value;
  var diemMon3 = document.getElementById("diem_mon_3").value;
  var diemKhuVuc = document.querySelector('input[name="khuVuc"]:checked').value;
  var doiTuong = document.querySelector(
    'input[name="doi_tuong"]:checked'
  ).value;
  var tongDiem =
    diemMon1 * 1 + diemMon2 * 1 + diemMon3 * 1 + diemKhuVuc * 1 + doiTuong * 1;
  // danh gia
  var danhGia = null;
  if (diemMon1 <= 0 || diemMon2 <= 0 || diemMon3 <= 0) {
    danhGia = "Rớt";
  } else {
    if (tongDiem >= diemChuan) {
      danhGia = "Đậu";
    } else {
      danhGia = "Rớt";
    }
  }
  document.getElementById(
    "thong_bao_ket_qua_diem"
  ).innerHTML = `${tongDiem} điểm`;
  document.getElementById("thong_bao_ket_qua").innerHTML = danhGia;
  console.log({
    diemKhuVuc,
    doiTuong,
    tongDiem,
    danhGia,
  });
});
